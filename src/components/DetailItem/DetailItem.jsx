import React from "react";
import PropTypes from "prop-types";

import styles from "./DetailItem.module.css";

DetailItem.propTypes = {
  detail: PropTypes.object,
};

function DetailItem(props) {
  const { detail } = props;
  const cutEmail = () => {
    return detail.from.slice(detail.from.indexOf("<"));
  };

  const timing = () => {
    let time = detail.date;
    let date = time.split("T")[0];
    let hour = time.split("T")[1];
     hour= hour.substr(0,8);
    return hour + " " + date;
  };
  console.log(detail);
  return (
    <div className="py-6 px-4">
      <div className="title flex flex-row">
        <div className={`${styles.account} flex`}>
          <div className={styles.avatar}>
            <img src="https://i.pravatar.cc/320?img=14" alt="" />
          </div>
          <div className="info">
            <p className="font-semibold text-md">{`${detail.senderName.first} ${detail.senderName.last}`}</p>
            <p className="text-sm text-gray-500 font-light">{cutEmail()}</p>
          </div>
        </div>
        <div className="actions flex items-center gap-4">
          <p className="text-sm text-gray-500 font-light ">{timing()}</p>
          <button
            className="flex items-center justify-center duration-100 shadow-md gap-2 px-3 py-2 text-sm rounded-md   
    bg-gray-500 text-white hover:bg-gray-400 false"
          >
            Reply
          </button>
          <button
            className="flex items-center justify-center duration-100 shadow-md gap-2 px-3 py-2 text-sm rounded-md   
    border border-green-500 text-green-500 hover:bg-green-200 false"
          >
            Forward
          </button>
          <button
            className="flex items-center justify-center duration-100 shadow-md gap-2 px-4 py-2 text-md rounded-md   
    border border-red-500 text-red-500 hover:bg-red-200 false"
          >
            Delete
          </button>
        </div>
      </div>
      <div className="body py-12">
        <h2 className="text-4xl font-semibold mb-8">{detail.subject}</h2>
        <div className="border-b pb-8">
          <p>{detail.body}</p>
        </div>
      </div>
    </div>
  );
}

export default DetailItem;
