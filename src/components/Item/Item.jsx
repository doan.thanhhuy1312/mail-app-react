import React from "react";
import PropTypes from "prop-types";

import styles from "./Item.module.css";

Item.propTypes = {
  info: PropTypes.object,
};

function Item(props) {
  const { info } = props;

  const formatDate = (date) => {
    const yyyy = date.slice(0, 4);
    const mm = date.slice(5, 7);
    const dd = date.slice(8, 10);
    return `${dd}/${mm}/${yyyy}`;
  };

  return (
    <div className={styles.divContainer}>
      <div className={styles.divAvatar}>
        <div className={styles.divImg}></div>
      </div>

      <div className={styles.divContent}>
        <div className="flex items-center justify-between w-full text-xs font-medium">
          <span>
            {info.senderName.first} {info.senderName.last}
          </span>
          <span>{formatDate(info.date.slice(0, 10))}</span>
        </div>

        <div
          className={`${styles.divTitle} text-md font-bold w-full line-clamp-1`}
        >
          <p>{info.subject}</p>
        </div>

        <div className={`${styles.divBody} w-full text-sm line-clamp-3`}>
          <p>{info.body}</p>
        </div>
      </div>
    </div>
  );
}

export default Item;
