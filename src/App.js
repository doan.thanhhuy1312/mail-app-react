import { Route, Routes } from "react-router-dom";
import Login from "./pages/Login/Login";
import Mail from "./pages/Mail/Mail";
import NavigateDashboard from "./pages/Navigate/NavigateDashboard";

import "./App.css";
import ProtectRoute from "./components/ProtectRoute/ProtectRoute";
import Item from "./components/Item/Item";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route element={<ProtectRoute />}>
          <Route path="main/home" element={<NavigateDashboard />} />
          <Route path="main/mail" element={<Mail />}>
            <Route path=":folder" element={<Mail />}>
              <Route path=":id" element={<Item/>}></Route>
            </Route>
          </Route>
          <Route path="main/contact" element={<NavigateDashboard />} />
        </Route>
        <Route path="*" element={<Login />} />
      </Routes>
    </div>
  );
}

export default App;
